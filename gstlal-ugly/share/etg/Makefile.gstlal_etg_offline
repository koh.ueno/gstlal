SHELL := /bin/bash
# condor commands
# Set the accounting tag from https://ldas-gridmon.ligo.caltech.edu/ldg_accounting/user
ACCOUNTING_TAG=ligo.dev.o3.detchar.onlinedq.idq
GROUP_USER=duncan.meacher
CONDOR_COMMANDS:=--condor-command=accounting_group=$(ACCOUNTING_TAG) --condor-command=accounting_group_user=$(GROUP_USER)

#########################
# Triggering parameters #
#########################

SEG_PAD = 1000

# The GPS start time for analysis
START = 1187000000
FSTART=$(shell echo $$((${START}-${SEG_PAD})))

# The GPS end time for analysis
STOP  = 1187100000

OUTPATH = $(PWD)
# Number of threads (N_channels x N_rates_per_channel) that each cpu will process
N_THREADS = 400
MISMATCH = 0.2
QHIGH = 40

# Detector
CLUSTER:=$(shell hostname -d)

IFO = H1
#IFO = L1

###############################
# Segment and frame type info #
###############################

# Info from https://wiki.ligo.org/viewauth/LSC/JRPComm/ObsRun2
# Select correct calibration type
# GSTLAL_SEGMENTS Options
SEG_SERVER=https://segments.ligo.org
# C00
LIGO_SEGMENTS="$(IFO):DMT-ANALYSIS_READY:1"
# C01
#LIGO_SEGMENTS="$*:DCS-ANALYSIS_READY_C01:1"
# C02
#LIGO_SEGMENTS="$*:DCS-ANALYSIS_READY_C02:1"

SEGMENT_TRIM = 0
SEGMENT_MIN_LENGTH = 512

FRAME_TYPE=R

#################
# Web directory #
#################

# A user tag for the run
#TAG = O2_C00
# Run number
#RUN = run_1
# A web directory for output (note difference between cit+uwm and Atlas)
# cit & uwm
#WEBDIR = ~/public_html/observing/$(TAG)/$(START)-$(STOP)-$(RUN)
# Atlas
#WEBDIR = ~/WWW/LSC/testing/$(TAG)/$(START)-$(STOP)-test_dag-$(RUN)

############
# Workflow #
############

all : dag
	sed -i '/gstlal_etg / s/$$/ |& grep -v '\''XLAL\|GSL\|Generic'\''/' etg_trigger_pipe.sh
	@echo "Submit with: condor_submit_dag etg_trigger_pipe.dag"

# Run etg pipe to produce dag
dag : frame.cache plots channel_list.txt segments.xml.gz
	./gstlal_etg_pipe \
		--data-source frames \
		--gps-start-time $(START) \
		--gps-end-time $(STOP) \
		--frame-cache frame.cache \
		--frame-segments-file segments.xml.gz \
		--frame-segments-name datasegments \
		--channel-list channel_list.txt \
		--out-path $(OUTPATH) \
		--threads $(N_THREADS)\
		--mismatch $(MISMATCH) \
		--qhigh $(QHIGH) \
		$(CONDOR_COMMANDS) \
		--request-cpu 2 \
		--request-memory 11GB \
		--verbose \
		--disable-web-service

#		--web-dir $(WEBDIR) \

# FIXME Determine channel list automatically.
#full_channel_list.txt : frame.cache
#	FrChannels $$(head -n 1 $^ | awk '{ print $$5}' | sed -e "s@file://localhost@@g") > $@

# Produce segments file
segments.xml.gz : frame.cache
	ligolw_segment_query_dqsegdb --segment-url=${SEG_SERVER} -q --gps-start-time ${FSTART} --gps-end-time ${STOP} --include-segments=$(LIGO_SEGMENTS) --result-name=datasegments > $@
	ligolw_cut --delete-column segment:segment_def_cdb --delete-column segment:creator_db --delete-column segment_definer:insertion_time $@
	gstlal_segments_trim --trim $(SEGMENT_TRIM) --gps-start-time $(FSTART) --gps-end-time $(STOP) --min-length $(SEGMENT_MIN_LENGTH) --output $@ $@

frame.cache :
	# FIXME force the observatory column to actually be instrument
	if [[ ${CLUSTER} == *"ligo-wa.caltech.edu" ]] ; then \
		gw_data_find -o H -t H1_$(FRAME_TYPE) -l  -s $(FSTART) -e $(STOP) --url-type file -O $@ ; \
	elif [[ ${CLUSTER} == *"ligo-la.caltech.edu" ]] ; then \
		gw_data_find -o L -t L1_$(FRAME_TYPE) -l  -s $(FSTART) -e $(STOP) --url-type file -O $@ ; \
	fi

# FIXME Add webpages once we have output
# Make webpage directory and copy files across
#$(WEBDIR) : $(MAKEFILE_LIST)
#	mkdir -p $(WEBDIR)/OPEN-BOX
#	cp $(MAKEFILE_LIST) $@

# Makes local plots directory
plots :
	mkdir plots

clean :
	-rm -rvf *.sub *.dag* *.cache *.sh logs *.sqlite plots *.html Images *.css *.js

