#!/usr/bin/env python
#
# Copyright (C) 2011-2017 Chad Hanna, Duncan Meacher
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""
This program makes a dag to run gstlal_etg offline
"""

__author__ = 'Duncan Meacher <duncan.meacher@ligo.org>'

##############################################################################
# import standard modules and append the lalapps prefix to the python path
import sys, os, stat
import itertools
import numpy
import math
from optparse import OptionParser

##############################################################################
# import the modules we need to build the pipeline
import lal
import lal.series
from lal.utils import CacheEntry
from glue import pipeline
from glue.lal import Cache
from glue import segments
from glue.ligolw import ligolw
from glue.ligolw import lsctables
import glue.ligolw.utils as ligolw_utils
import glue.ligolw.utils.segments as ligolw_segments
from gstlal import inspiral, inspiral_pipe
from gstlal import dagparts as gstlaldagparts
from gstlal import datasource
from gstlal import multichannel_datasource
from gstlal import idq_multirate_datasource
from gstlal import idq_aggregator

class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass
lsctables.use_in(LIGOLWContentHandler)


#
# get a dictionary of all the segments
#

def breakupseg(seg, maxextent, overlap):
	if maxextent <= 0:
		raise ValueError, "maxextent must be positive, not %s" % repr(maxextent)

	# Simple case of only one segment
	if abs(seg) < maxextent:
		return segments.segmentlist([seg])

	# adjust maxextent so that segments are divided roughly equally
	maxextent = max(int(abs(seg) / (int(abs(seg)) // int(maxextent) + 1)), overlap)
	maxextent = int(math.ceil(abs(seg) / math.ceil(abs(seg) / maxextent)))
	end = seg[1]

	seglist = segments.segmentlist()

	while abs(seg):
		if (seg[0] + maxextent + overlap) < end:
			# Round down segment gps end time to integer multiple of cadence.
			seglist.append(segments.segment(seg[0], idq_aggregator.floor_div(int(seg[0]) + maxextent + overlap, options.cadence)))
			seg = segments.segment(seglist[-1][1] - overlap, seg[1])
		else:
			seglist.append(segments.segment(seg[0], end))
			break

	return seglist

def breakupsegs(seglist, maxextent, overlap):
	newseglist = segments.segmentlist()
	for bigseg in seglist:
		newseglist.extend(breakupseg(bigseg, maxextent, overlap))
	return newseglist

def analysis_segments(ifo, allsegs, boundary_seg, max_template_length = 30):
	segsdict = segments.segmentlistdict()
	# 512 seconds for the whitener to settle + the maximum template_length
	start_pad = idq_multirate_datasource.PSD_DROP_TIME + max_template_length
	# Chosen so that the overlap is only a ~5% hit in run time for long segments...
	segment_length = int(10 * start_pad)

	segsdict[ifo] = segments.segmentlist([boundary_seg])
	segsdict[ifo] = segsdict[ifo].protract(start_pad)
	# FIXME revert to gstlaldagparts.breakupsegs and remove above two functions when we no longer write to ascii.
	segsdict[ifo] = breakupsegs(segsdict[ifo], segment_length, start_pad)
	if not segsdict[ifo]:
		del segsdict[ifo]

	return segsdict

#
# get a dictionary of all the channels per gstlal_etg job
#

def etg_node_gen(gstlalETGJob, dag, parent_nodes, segsdict, ifo, options, channels, data_source_info):
	etg_nodes = {}
	cumsum_rates = 0
	total_rates = 0
	outstr = ""
	n_channels = 0
	n_process = 0
	trig_start = options.gps_start_time
	write_channel_list = True

	# Loop over all channels to determine number of threads and minimum number of processes needed
	for ii, channel in enumerate(channels,1):
		samp_rate = data_source_info.channel_dict[channel]['fsamp']
		max_samp_rate = min(2048, int(samp_rate))
		min_samp_rate = min(32, max_samp_rate)
		n_rates = int(numpy.log2(max_samp_rate/min_samp_rate) + 1)
		cumsum_rates += n_rates
		total_rates += n_rates

		if cumsum_rates >= options.threads or ii == len(data_source_info.channel_dict.keys()):
			n_process += 1
			cumsum_rates = 0

	# Create more even distribution of channels across minimum number of processes
	n_threads = math.ceil(total_rates / n_process)

	if options.verbose:
		print "Total threads =", total_rates
		print "Total jobs needed =", n_process
		print "Evenly distributed threads per job =", int(n_threads)

	for seg in segsdict[ifo]:

		cumsum_rates = 0
		out_index = 0
		channel_list = []

		for ii, channel in enumerate(channels,1):
			n_channels += 1
			samp_rate = data_source_info.channel_dict[channel]['fsamp']
			max_samp_rate = min(2048, int(samp_rate))
			min_samp_rate = min(32, max_samp_rate)
			n_rates = int(numpy.log2(max_samp_rate/min_samp_rate) + 1)
			cumsum_rates += n_rates
			outstr = outstr + channel + ":" + str(int(samp_rate))
			channel_list.append(channel + " " + str(int(samp_rate)))
			outpath = os.path.join(options.out_path, "gstlal_etg")

			# Adds channel to current process
			if cumsum_rates < n_threads and ii < len(data_source_info.channel_dict.keys()):
				outstr = outstr + " --channel-name="

			# Finalise each process once number of threads passes threshold
			if cumsum_rates >= n_threads or ii == len(data_source_info.channel_dict.keys()):
				out_index += 1
				etg_nodes[channel] = \
					inspiral_pipe.generic_node(gstlalETGJob, dag, parent_nodes = parent_nodes,
						opts = {"gps-start-time":int(seg[0]),
							"gps-end-time":int(seg[1]),
							"trigger-start-time":int(trig_start),
							"trigger-end-time":int(seg[1]),
							"data-source":"frames",
							"channel-name":outstr,
							"mismatch":options.mismatch,
							"qhigh":options.qhigh,
							"cadence":options.cadence,
							"job-id": out_index,
							"disable-web-service":"",
							"frame-segments-name": options.frame_segments_name,
							"save-hdf": options.save_hdf
						},
						input_files = {"frame-cache":options.frame_cache,
							"frame-segments-file":options.frame_segments_file},
						output_files = {"out-path":outpath}
					)
				if options.verbose and write_channel_list is True :
					print "Job %04d, number of channels = %3d, number of threads = %4d" %(out_index, n_channels, cumsum_rates)

				if write_channel_list is True :
					listpath = options.out_path + "/gstlal_etg/channel_lists/channel_list_%04d.txt" %(out_index)
					f = open(listpath,'w')
					for channel_out in channel_list:
						f.write(channel_out+'\n')
					f.close()

				cumsum_rates = 0
				outstr = ""
				n_channels = 0
				channel_list = []

		trig_start = int(seg[1])
		write_channel_list = False

	return etg_nodes

#
# Main
#

def parse_command_line():
	parser = OptionParser(description = __doc__)

	# generic data source options
	#datasource.append_options(parser)
	multichannel_datasource.append_options(parser)

	# trigger generation options
	parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose.")
	parser.add_option("--triggers-from-dataframe", action = "store_true", default = False,
		help = "If set, will output iDQ-compatible triggers to disk straight from dataframe once every cadence")
	parser.add_option("--disable-web-service", action = "store_true", help = "If set, disables web service that allows monitoring of PSDS of aux channels.")
	parser.add_option("--description", metavar = "string", default = "GSTLAL_IDQ_TRIGGERS", help = "Set the filename description in which to save the output.")
	parser.add_option("--cadence", type = "int", default = 32, help = "Rate at which to write trigger files to disk. Default = 32 seconds.")
	parser.add_option("-m", "--mismatch", type = "float", default = 0.2, help = "Mismatch between templates, mismatch = 1 - minimal match. Default = 0.2.")
	parser.add_option("-q", "--qhigh", type = "float", default = 20, help = "Q high value for half sine-gaussian waveforms. Default = 20.")
	parser.add_option("-t", "--threads", type = "float", default = 100, help = "Number of threads to process per node. Default = 100.")
	parser.add_option("-l", "--latency", action = "store_true", help = "Print latency to output ascii file. Temporary.")
	parser.add_option("--save-hdf", action = "store_true", default = False, help = "If set, will save hdf5 files to disk straight from dataframe once every cadence")
	parser.add_option("--out-path", metavar = "path", default = ".", help = "Write to this path. Default = .")

	# Condor commands
	parser.add_option("--request-cpu", default = "2", metavar = "integer", help = "set the inspiral CPU count, default = 2")
	parser.add_option("--request-memory", default = "7GB", metavar = "integer", help = "set the inspiral memory, default = 7GB")
	parser.add_option("--condor-command", action = "append", default = [], metavar = "command=value", help = "set condor commands of the form command=value; can be given multiple times")

	options, filenames = parser.parse_args()

	return options, filenames

#
# Useful variables
#

options, filenames = parse_command_line()

output_dir = "plots"

listdir = os.path.join(options.out_path, "gstlal_etg/channel_lists")
if not os.path.exists(listdir):
    os.makedirs(listdir)

#
#
#

data_source_info = multichannel_datasource.DataSourceInfo(options)
ifo = data_source_info.instrument
channels = data_source_info.channel_dict.keys()
boundary_seg = data_source_info.seg

# FIXME Work out better way to determine max template length
max_template_length = 30

#
# Setup the dag
#

try:
	os.mkdir("logs")
except:
	pass
dag = inspiral_pipe.DAG("etg_trigger_pipe")

#
# setup the job classes
#

gstlalETGJob = inspiral_pipe.generic_job("gstlal_etg", condor_commands = inspiral_pipe.condor_command_dict_from_opts(options.condor_command, {"request_memory":options.request_memory, "request_cpus":options.request_cpu, "want_graceful_removal":"True", "kill_sig":"15"}))

segsdict = analysis_segments(ifo, data_source_info.frame_segments, boundary_seg, max_template_length)

#
# ETG jobs
#

etg_nodes = etg_node_gen(gstlalETGJob, dag, [], segsdict, ifo, options, channels, data_source_info)

#
# all done
#

dag.write_sub_files()
dag.write_dag()
dag.write_script()
